from typing import List
from time import time
from email_validator import validate_email, EmailNotValidError
from fastapi import Depends, FastAPI, HTTPException, status
from bcrypt import checkpw
from sqlalchemy.orm import Session

from . import crud, models, schemas
from .database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

def sanitise_email(email):
    email_info = validate_email(email, check_deliverability=False)
    return email_info.normalized

@app.post("/api/register/", response_model=schemas.EmailVerification)
def create_user(user: schemas.UserCreate, db: Session = Depends(get_db)):
    try:
        user.email = sanitise_email(email=user.email)
        db_user = crud.get_user_by_email(db, email=user.email)
        if db_user:
            raise HTTPException(status_code=400, detail="Email already registered")
        db_user = crud.create_user(db=db, user=user)
        db_email_verification = crud.create_email_verification(db, user_id=db_user.id)
        # send_verification_email(db_email_verification)
        return db_email_verification # Return code to allow testing verify endpoint 
    except EmailNotValidError as e:
        raise HTTPException(status_code=400, detail="Invalid email address")

@app.post("/api/login/", response_model=schemas.User)
def login(user: schemas.UserCreate, db: Session = Depends(get_db)):
    db_user = crud.get_user_by_email(db, email=user.email)
    if db_user and checkpw(user.password.encode('utf-8'), db_user.hashed_password):
        return db_user
    else:
        raise HTTPException(status_code=400, detail="Email or Password incorrect")

@app.get("/api/verify/{token}", status_code=status.HTTP_200_OK)
def verify(token: str, db: Session = Depends(get_db)):
    db_email_verification = crud.get_email_verification(db, token=token);
    now = int(time());
    if db_email_verification and db_email_verification.valid_until > now:
        crud.verify_user(db, db_email_verification.user_id)
    else:
        raise HTTPException(status_code=400, detail="Verification code invalid")
    return
