from typing import Optional
from pydantic import BaseModel, ConfigDict

class UserBase(BaseModel):
    email: str

class UserCreate(UserBase):
    password: str

class User(UserBase):
    model_config = ConfigDict(from_attributes=True)

    id: int
    verified: bool


class EmailVerificationBase(BaseModel):
    token: str
    valid_until: int

class EmailVerificationCreate(BaseModel):
    pass

class EmailVerification(EmailVerificationBase):
    model_config = ConfigDict(from_attributes=True)

    id: int
    user_id: int
