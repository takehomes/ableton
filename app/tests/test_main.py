from fastapi.testclient import TestClient
from time import time

from ..main import app

client = TestClient(app)

def test_create_user():
    email = "testemail" + str(time()) + "@test.com"
    response = client.post(
        "/api/register",
        json={"email": email, "password": "p@ssw0rd"},
    )
    assert response.status_code == 200
    json = response.json();
    assert type(json['valid_until']) is int
    assert type(json['id']) is int
    assert type(json['user_id']) is int
    assert type(json['token']) is str
    assert len(json['token']) == 128
