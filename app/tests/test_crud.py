from fastapi.testclient import TestClient
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Session
from sqlalchemy.pool import StaticPool
import pytest

from ..database import Base
from ..crud import create_email_verification

SQLALCHEMY_DATABASE_URL = 'sqlite://'

engine = create_engine(
    SQLALCHEMY_DATABASE_URL,
    connect_args={"check_same_thread": False},
    poolclass=StaticPool,
)
Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)


@pytest.fixture(scope="module")
def db_session():
    Base.metadata.create_all(bind=engine)
    session = Session()
    yield session
    session.rollback()
    session.close()


class TestCRUD:
    def test_create_email_verification(self, db_session):
        user_id = 12345;
        db_verf = create_email_verification(db_session, user_id)
        assert type(db_verf.valid_until) is int
        assert type(db_verf.id) is int
        assert type(db_verf.user_id) is int
        assert type(db_verf.token) is str
        assert len(db_verf.token) == 128
