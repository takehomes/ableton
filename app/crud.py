import random
from time import time
from string import ascii_letters
from sqlalchemy.orm import Session
from bcrypt import hashpw, gensalt, checkpw
from pydantic import parse_obj_as

from . import models, schemas

def get_user(db: Session, user_id: int):
    return db.query(models.User).filter(models.User.id == user_id).first()

def get_user_by_email(db: Session, email: str):
    return db.query(models.User).filter(models.User.email == email).first()

def create_user(db: Session, user: schemas.UserCreate):
    hashed_pw = hashpw(user.password.encode('utf-8'), gensalt())
    db_user = models.User(email=user.email, hashed_password=hashed_pw)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user

def verify_user(db: Session, user_id: int):
    db_user = get_user(db, user_id)
    if db_user:
        db_user.verified = True
        db.commit()
        db.refresh(db_user)
    return db_user;

def get_email_verification(db: Session, token: str):
    return db.query(models.EmailVerification).filter(models.EmailVerification.token == token).first()

def create_email_verification(db: Session, user_id: int):
    token = ''.join(random.choice(ascii_letters) for i in range(128))
    valid_until = int(time()) + 24 * 60 * 60; # now + 24 hours
    db_email_verification = models.EmailVerification(token=token, user_id=user_id, valid_until=valid_until)
    db.add(db_email_verification)
    db.commit()
    db.refresh(db_email_verification)
    return db_email_verification
