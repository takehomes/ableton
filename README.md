Install with `pip install -r requirements.txt`

Run with `uvicorn app.main:app --reload`

View and test endpoints at `http://127.0.0.1:8000/docs#/`

Run tests with `pytest`

## Explanation of solution
### POST /api/register
- New user provides an email and password via POST request
- Email input is sanitised to prevent any SQL injections
  - SQLAlchemy should guard against this so it's likely overkill, but _just in case_  
- Database is queried to check if a user with that email already exists, returns early with `400` if so
- If user does not exist it creates a new user
  - At create the password is hashed and salted using the bcrypt library
- It then creates a new verification code for that user
- _[At this stage any email sending logic would be run]_
- Verification code is then returned with the call
  - In a real world API we would likely return the new user object and/or a `201` code as the code would be delivered via email
  - For the sake of this exercise I'm returning the verification code so that the `/api/verify` endpoint can be tested without needing to access the db directly

### POST /api/login
- Existing user provides email and password via POST request
- Check to see if user exists, and if password matches existing password using bcrypt
- If not return a `400`
  - Depending on security concerns we may be okay with providing more feedback regarding the nature of the login error rather than just a generic `400`
- Return the user object

### GET /api/verify
- User provides a verification token
- We check to see if this verification token exists in the db and it hasn't expired
- If so we update the corresponding user and set their verified status to TRUE
- Return a `200` if successful, `400` otherwise

We want to use GET here so that a user can use this endpoint by just clicking on the link in their email. We may want to remove the `/api/` part and have this behave like a webpage rather than a REST endpoint.

## What I would consider next
#### Verification refresh
For this solution I created a TTL for the verification code, this is good practice for password reset links but it's arguable whether it's strictly necessary for signup. One benefit is that it would allow building a cron to clear up stale users who never completed signup.

As a result of this we would need to also create a way for users to get a fresh verification code if they didn't use it before expiration. This hasn't been implemented here.

#### JWT access token
Sessions were out of the scope of this exercise but it is customary to return an access token with login endpoints that clients can use then use in subsequent calls without having to pass the username and password with every call.

#### Password validation
Depending on the use case it would also be wise to implement some minimum password requirements, in this solution I've allowed any password, including empty strings.

#### Verification code instead of url
With a url-type verification there is the chance that a user might use a junk email that happens to match an existing email, that email owner could then click on the verification link, which would cause the email to be verified without it _actually_ being verified.

A way to avoid this would be to provide a code-style verification where a 6 digit number or random letters are sent to the user and they have to input it manually into a provided input. This would ensure that the user has to have access to the email provided.

### More tests
Minimal tests were written for two modules two demonstrate two different setups for unit tests. It includes two happy-path tests but ideally edge cases and unhappy paths would be included also. In a real application all (or close to all) methods would have unit tests along with any other appropriate automated testing such as integration and end-to-end testing.

#### Split files
The codebase here is fairly small and fairly straightforward but if it were to grow any larger it would require some file splitting to seperate the business logic in main.py into its own services and utils modules to make everything more manageable and testable.

## General Notes
- Unfortunately I did not read the assignment instructions correctly before I had largely completed my solution. I had missed the _"Please only use Python 3 standard libraries in your solution"_ as should be immediately obvious by the addition of a `requirements.txt`, and do not have enough time to refactor it to remove the non-standard libraries used. I hope that the submitted solution should still give an adequate indication of my coding abilities, even if it was comepleted not exactly as-asked. If necessary I'm willing to complete a different coding test if being able to complete a solution with only standard libraries is considered important for this role.
- I did not use any AI tools for any of the code in this solution though the project structure is largely based on the following FastAPI guide [https://fastapi.tiangolo.com/tutorial/sql-databases/](https://fastapi.tiangolo.com/tutorial/sql-databases/)
- I don't have any python linters installed on this machine so apologies for any inconsistent styling. This is my first time working on a fully python-based app in a while so I do not have my usual setup configured.
